<?php
    session_start();
    $id_equipment = $_GET['id_equipment'];
    require_once '/Applications/MAMP/htdocs/vendor/config/connect.php';

    $sql = "SELECT s.name as brand_name, m.name as model_name, equipment.name as equipment_name, description, price, p.link as photo_link FROM equipment 
            JOIN photo p on equipment.id_equipment = p.id_equipment 
            JOIN model m on equipment.id_model = m.id_model
            JOIN stamp s on m.id_stamp = s.id_stamp
            WHERE equipment.id_equipment = ".$id_equipment.";";
    $services = mysqli_query($connect, $sql);
    $row = mysqli_fetch_array($services);

    $description = $row['description'];
    $model_name = $row['model_name'];
    $brand_name = $row['brand_name'];
    $equipment_name = $row['equipment_name'];
    $price = $row['price'];
    $photo_link = $row['photo_link'];

    $sql_property = "SELECT p.name as name, value FROM equipment_property JOIN property p on equipment_property.id_property = p.id_property WHERE equipment_property.id_equipment = ".$id_equipment.";";
    $services_property = mysqli_query($connect, $sql_property);

    $sql_additional_service = "
        SELECT `as`.name as additional_service_name, price
        FROM equipment_additional_service
            JOIN additional_service `as` on equipment_additional_service.id_additional_service = `as`.id_additional_service
        WHERE equipment_additional_service.id_equipment = ".$id_equipment.";";
    $services_additional_service = mysqli_query($connect, $sql_additional_service);

    $sql_color = "
        SELECT value
        FROM equipment_body_color
            JOIN body_color bc on equipment_body_color.id_body_color = bc.id_body_color
        WHERE id_equipment = ".$id_equipment.";";
    $services_color = mysqli_query($connect, $sql_color);

    ?>
<script src="../../assets/js/getOrder.js"></script>
    <div class="container">
        <div class="row">

            <div id="pic">
                <img src=../../<?=$photo_link?> width="392" height="222"
                     alt="Автомобиль">
            </div>
            <div id="name_brand">
                <h1><?=$brand_name?></h1>
                <h2><?=$model_name.' '.$equipment_name?></h2>
                <h2 id="price_car"> От <?=$price.' ₽'?></h2>
                
                <input type="submit" value="Купить" onclick="choiceColor(<?=$id_equipment?>)">
                <div id="colorList"></div>
                
               
            </div>
        </div>
        <div class="row">
            <h3>Описание</h3>
            <div id="text"><?=$description?></div>
        </div>

        <div class="row">

            <h3>Характеристики</h3>

            <table>
                <tr>
                    <th> Название характеристики </th>
                    <th> Значение </th>
                </tr>

                <?php
                while ($row_property = mysqli_fetch_array($services_property)) {

                    $name_property = $row_property['name'];
                    $value_property = $row_property['value'];

                    $result ='';
                    $result .= '<tr>';
                    $result .= '<td>' . $name_property . '</td> <td>' . $value_property . '</td>';
                    $result .= '</tr>';
                    echo $result;
                }
            ?>
            </table>
        </div>

        <div class="row">
                <?php
                $row_additional_service = mysqli_fetch_array($services_additional_service);
                if (!empty($row_additional_service)) {
                    $result = '<h3>Дополнительные услуги</h3> 
                               <table>
                                    <tr>
                                        <th> Название дополнительной услуги </th>
                                        <th> Цена </th>
                                    </tr>';
                    $result .= '<tr>';
                    $result .= '<td>' . $row_additional_service['additional_service_name'] . '</td> <td>' . $row_additional_service['price'] . '</td>';
                    $result .= '</tr>';
                    echo $result;
                    while ($row_additional_service = mysqli_fetch_array($services_additional_service)) {

                        $result ='';
                        $result .= '<tr>';
                        $result .= '<td>' . $row_additional_service['additional_service_name'] . '</td> <td>' . $row_additional_service['price'] . '</td>';
                        $result .= '</tr>';
                        echo $result;
                    }
                    echo '</table>';
                }
                ?>
        </div>

        <div class="row">
            <h3>Доступные цвета</h3>

            <table>
                <?php
                while ($row_color = mysqli_fetch_array($services_color)) {
                    $result ='';
                    $result .= '<tr>';
                    $result .= '<td>' . $row_color['value'] . '</td>';
                    $result .= '</tr>';
                    echo $result;
                }
                ?>
            </table>
        </div>
    </div>';




