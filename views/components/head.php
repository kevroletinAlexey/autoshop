<?php
    error_reporting(0);
    ?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Автосалон</title>
    <link href="../../assets/css/styleHome.css" rel="stylesheet">

</head>

<body>
<header class="site-header">
    <nav class="site-navigation">

        <a class="logo-link" href="index.php">
            <img src="../../assets/img/animirovanii_logo.png" width="62" height="17" alt="Логотип магазина gloevk">
        </a>


        <ul class="navigation-list">


            <div class="dropdown">
                <button class="dropbtn">Каталог
                    <i class="fa fa-caret-down"></i>
                </button>
                <div class="dropdown-content">
                    <li><a href="catalog.php?brand=1" >Porsche</a></li>
                    <li><a href="catalog.php?brand=2" >Jeep</a></li>
                    <li><a href="#" onclick="getCars('BMW')">BMW</a></li>
                </div>
            </div>

            <?php
            if (isset($_SESSION['user'])) {
                echo "<li><a href='clientOrder.php'>Мои заказы</a></li>";
            }
            if (isset($_SESSION['manager'])) {
                echo "<li><a href='managerOrder.php'>Заказы</a></li>";
                echo "<li><a href='addCar.php'>Добавить автомобиль</a></li>";
                echo "<li><a href='carsStock.php'>Автомобили на складе</a></li>";
            } ?>
            <li><a href='#'>Кредит</a></li>
            <li><a href='#'>О нас</a></li>

            <?php
            if (isset($_SESSION['user']) or isset($_SESSION['manager'])) {
                echo "<li><a href='../../vendor/authorization/logout.php'>Выход</a></li>";
            } else {
                echo "<li><a href='../../views/pages/login.php'>Вход</a></li>";
            }
            ?>

        </ul>
    </nav>
    <?php
    if(isset($_SESSION['user'])) {
        echo"<script src='../../assets/js/closeSession.js'> </script>";
    }
    else if(isset($_SESSION['manager'])){
        echo"<script src='../../assets/js/closeSession.js'> </script>";
    }
    ?>

</header>

