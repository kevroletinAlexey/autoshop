<?php
    session_start();
    $id_manager = $_SESSION['manager']['id'];
?>

<script src="../../assets/js/addCar.js"></script>

<section class="catalog">
<div class="container">
    <div class="row">
        <h1>Добавление автомобиля в автосалон</h1>
        <input type="submit" value="Добавить автомобиль" onclick="addNewCar(<?=$id_manager?>)">
        <div id="brandList"></div>
    </div>
</div>
</section>
