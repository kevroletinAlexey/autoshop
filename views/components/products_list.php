
<section class="catalog">
    <div class="container">
        <h1 class="catalog-title">Каталог товаров</h1>
        <div id="carList">
            <?php
            require_once '/Applications/MAMP/htdocs/vendor/config/connect.php';

            $brand = $_GET['brand'];

            $sql = "SELECT model.name as model_name, e.name as equipment_name, e.description, e.price, link, e.id_equipment as id_equipment
            FROM model 
            JOIN equipment e on model.id_model = e.id_model 
            JOIN stamp s on model.id_stamp = s.id_stamp 
            JOIN photo p on e.id_equipment = p.id_equipment
            WHERE s.id_stamp = '".$brand."';";

            $services = mysqli_query($connect, $sql);

            ?>

            <ul class="products-list">

                <?php
                while ($row = mysqli_fetch_array($services)) {

                    $id_equipment = $row['id_equipment'];
                    $model_name = $row['model_name'];
                    $equipment_name = $row['equipment_name'];
                    $price = $row['price'];
                    $link = $row['link'];
                    echo "<li>
                <a class='product-card' href='equipment.php?id_equipment=".$id_equipment."'>
                    <h3>".$model_name."</h3>";
                    echo "<p>".$equipment_name."</p>";
                    echo '      <span class="price">' .$price. '</span>
                    <img src=../../'.$link.' width="195" height="120" alt="'.$equipment_name.'">
                </a>
            </li>';
                }
                ?>
            </ul>
        </div>
    </div>
</section>
