<?php
    session_start();
    error_reporting(0);
    if (!$_SESSION['user']) {
        header('Location: index.php');
    }
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <title>Car dealership</title>
</head>
<body>

    <form>
        <h2 style="margin: 10px 0;">
            <?= $_SESSION['user']['full_name'] ?>
        </h2>
        <a href="#">
            <?= $_SESSION['user']['email'] ?>
        </a>
        <a href="../../vendor/authorization/logout.php" class="logout">
            Выход
        </a>
    </form>

</body>
</html>