<?php
session_start();
error_reporting(0);
if ($_SESSION['user'] or $_SESSION['manager']) {
    header('Location: index.php');
}
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <title>Car dealership</title>
</head>
<body>

<!-- Форма авторизации -->

<form>
    <label>Логин</label>
    <input type="text" name="login" placeholder="Введите свой логин">
    <label>Пароль</label>
    <input type="password" name="password" placeholder="Введите свой пароль">

    <button type="submit" class="login-btn">Войти</button>
    <p>
        У Вас нет аккаунта? - <a href="register.php">зарегестрируйтесь</a>
    </p>
    <p class="msg none"></p>
</form>

<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/main.js"></script>

</body>
</html>