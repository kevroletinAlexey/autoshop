<?php
session_start();
require('../components/head.php');
?>
    <script src="../../assets/js/setCookie.js"></script>

    <section class="catalog">
        <?php
            if (isset($_SESSION["user"])) {
                echo "<select name='id_city' onchange='setCookie(this.value)'>
                      <option value=''>Выберите город:</option>";
                include_once "../../vendor/getCity.php";

                while ($row = mysqli_fetch_array($services)) {
                    echo "<option value=".$row['id_city'].">".$row['name']."</option>";
                }

                echo "</select>
                        <div id='setCookieList'></div>";
            }

            //если куки установлен, но хочется поменять город
            /*
            if ((isset($_COOKIE["id_city"])) AND (isset($_SESSION["user"]))) {
                echo "<form>
                        <button type='submit' onclick='changeCookie()'>Изменить город</button>
                      </form>";
                echo "<div id='changeCookieList'></div>";
            }
*/

        ?>

        <div class="container">
            <h1 class="catalog-title">Главная страница</h1>
        </div>

    </section>

<?php
require('../components/footer.php');
?>


