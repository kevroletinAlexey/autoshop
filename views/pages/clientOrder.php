<?php
session_start();
require('../components/head.php');
require_once '/Applications/MAMP/htdocs/vendor/config/connect.php';

$query = "SELECT s.name AS brand_name, m.name AS model_name, e.name AS equipment_name, data_create, bc.value AS color,
       c.name AS city, a.street AS street, a.house AS house,
       m2.last_name AS manager_last_name, m2.first_name AS manager_first_name, m2.patronymic AS manager_patronymic,
       m2.phone AS manager_phone, id_client_order
FROM client_order
    JOIN car_branch cb on client_order.id_car_branch = cb.id_car_branch
    JOIN equipment e on cb.id_equipment = e.id_equipment
    JOIN model m on e.id_model = m.id_model
    JOIN stamp s on m.id_stamp = s.id_stamp
    JOIN body_color bc on cb.id_body_color = bc.id_body_color
    JOIN branch b on cb.id_branch = b.id_branch
    JOIN address a on b.id_address = a.id_address
    JOIN city c on a.id_city = c.id_city
    JOIN manager m2 on client_order.id_manager = m2.id_manager
WHERE client_order.id_user = ".$_SESSION['user']['id'].";";

$services = mysqli_query($connect, $query);


?>

<section class="catalog">
    <div class="orders">
        <div class="row">
            <h3>Заказы</h3>
            <table id="tableClientOrder">
                <tr>
                    <th>Название автомобиля</th>
                    <th>Цвет кузова</th>
                    <th>Доп. услуги</th>
                    <th>Дата создания заказа</th>
                    <th>Адрес автосалона</th>
                    <th>Менеджер</th>
                    <th>Телефон менеджера</th>
                </tr>

                <?php
                while ($row = mysqli_fetch_array($services)) {
                    $result ='';
                    $result .= '<tr>';
                    $result .= '<td>'.$row['brand_name'].' '.$row['model_name'].' '.$row['equipment_name'].'</td>';
                    $result .= '<td>'.$row['color'].'</td>';

                    $query_coas = "
                            SELECT `as`.name AS coas_name FROM client_order
                            JOIN client_order_additional_service coas on client_order.id_client_order = coas.id_client_order
                            JOIN additional_service `as` on coas.id_additional_service = `as`.id_additional_service
                            WHERE client_order.id_user = ".$_SESSION['user']['id']." AND coas.id_client_order = ".$row['id_client_order'].";";
                    $services_coas = mysqli_query($connect, $query_coas);

                    $coas = '';
                    while ($row_coas = mysqli_fetch_array($services_coas)) {
                        $coas .= '<li>'.$row_coas['coas_name'].'</li>';
                    }
                    $result .= '<td> <ul>'.$coas.'</ul> </td>';

                    $result .= '<td>'.$row['data_create'].'</td>';
                    $result .= '<td>'.$row['city'].' '.$row['street'].' '.$row['house'].'</td>';
                    $result .= '<td>'.$row['manager_last_name'].' '.$row['manager_first_name'].' '.$row['manager_patronymic'].'</td>';
                    $result .= '<td>'.$row['manager_phone'].'</td>';
                    $result .= '</tr>';
                    echo $result;
                }
                ?>

            </table>
        </div>
    </div>
</section>

<?php
require('../components/footer.php');
?>
