<?php
session_start();
require('../components/head.php');
require_once '/Applications/MAMP/htdocs/vendor/config/connect.php';



$query = "
SELECT s.name AS brand_name, m.name AS model_name, e.name AS equipment_name, car_branch.vin AS vin,
       scb.value AS status, bc.value AS color
    FROM car_branch
JOIN equipment e on car_branch.id_equipment = e.id_equipment
JOIN model m on e.id_model = m.id_model
JOIN stamp s on m.id_stamp = s.id_stamp
JOIN body_color bc on car_branch.id_body_color = bc.id_body_color
JOIN status_car_branch scb on car_branch.id_status_car_branch = scb.id_status_car_branch
WHERE car_branch.id_branch = ".$_SESSION['manager']['id_branch'].";";

$services = mysqli_query($connect, $query);


?>

<section class="catalog">
    <div class="orders">
        <div class="row">
            <h3>Автомобили на складе</h3>
            <table id="tableClientOrder">
                <tr>
                    <th>Название автомобиля</th>
                    <th>Цвет кузова</th>
                    <th>VIN</th>
                    <th>Статус</th>
                </tr>

                <?php
                while ($row = mysqli_fetch_array($services)) {
                    $result ='';
                    $result .= '<tr>';
                    $result .= '<td>'.$row['brand_name'].' '.$row['model_name'].' '.$row['equipment_name'].'</td>';
                    $result .= '<td>'.$row['color'].'</td>';
                    $result .= '<td>'.$row['vin'].'</td>';
                    $result .= '<td>'.$row['status'].'</td>';
                    $result .= '</tr>';
                    echo $result;
                }
                ?>

            </table>
        </div>
    </div>
</section>

<?php
require('../components/footer.php');
?>

