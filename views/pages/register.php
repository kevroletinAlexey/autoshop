<?php

session_start();
error_reporting(0);
if ($_SESSION['user']) {
    header('Location: index.php');
}
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <title>Car dealership</title>
</head>
<body>

<!-- Форма регистрации -->

<form>
    <label>ФИО</label>
    <input type="text" name="full_name" placeholder="Введите свое полное имя">
    <label>Почта</label>
    <input type="email" name="email" placeholder="Введите свою почту">
    <label>Телефон</label>
    <input type="phone" name="phone" placeholder="Введите свой номер телефона">
    <label>Логин</label>
    <input type="text" name="login" placeholder="Введите свой логин">
    <label>Пароль</label>
    <input type="password" name="password" placeholder="Введите пароль">
    <label>Потверждение пароля</label>
    <input type="password" name="password_confirm" placeholder="Потвердите пароль">
    <button type="submit" class="register-btn">Зарегистрироваться</button>
    <p>
        У Вас уже есть аккаунта? - <a href="login.php">Авторизируйтесь</a>
    </p>
    <p class="msg none"></p>
</form>

<script src="../../assets/js/jquery-3.6.0.min.js"></script>
<script src="../../assets/js/main.js"></script>


</body>
</html>
