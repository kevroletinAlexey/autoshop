<?php
session_start();
if(!isset($_SESSION['user'])){
    header("location: login.php");
}

require('../components/head.php');
require('../components/equipment_info.php');
require('../components/footer.php');
?>
