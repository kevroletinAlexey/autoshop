function choiceColor(id_equipment) {
    if (id_equipment == "") {
        document.getElementById("colorList").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("colorList").innerHTML = this.responseText;
            }
        };
        console.log(id_equipment);
        xmlhttp.open("GET","../../vendor/makeOrder/getColor.php?id_equipment="+id_equipment,true);
        xmlhttp.send();
    }
}

function getBranch(id_equipment, id_body_color) {
    if (id_body_color == "") {
        document.getElementById("branchList").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("branchList").innerHTML = this.responseText;
            }
        };
        console.log(id_equipment, id_body_color);
        xmlhttp.open("GET","../../vendor/makeOrder/getBranch.php?id_equipment="+id_equipment+"&id_body_color="+id_body_color,true);
        xmlhttp.send();
    }
}

function getAdditionalService(id_equipment, id_body_color, id_branch) {
    if (id_branch == "") {
        document.getElementById("additionalServiceList").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("additionalServiceList").innerHTML = this.responseText;
            }
        };

        xmlhttp.open("GET","../../vendor/makeOrder/getAdditionalService.php?id_equipment="+id_equipment+"&id_body_color="+id_body_color+"&id_branch="+id_branch,true);
        xmlhttp.send();
    }
}


function makeOrder(id_equipment, id_body_color, id_branch){

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let id_client_order = this.responseText;

            let elements = document.querySelectorAll('input.get_additional_service');

            for (let elem of elements) {
                if (elem.checked) {
                    const url = "../../vendor/makeOrder/pushAdditionalService.php"
                    const request = new XMLHttpRequest();
                    const params = "id_additional_service="+elem.value+"&id_client_order="+id_client_order;

                    request.open("POST", url, true);
                    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    request.addEventListener("readystatechange", () => {
                        if(request.readyState === 4 && request.status === 200) {
                            location.href = "clientOrder.php";
                        }
                    });
                    request.send(params);
                }
            }
        }
    };

    xmlhttp.open("GET","../../vendor/makeOrder/makeOrder.php?id_equipment="+id_equipment+"&id_body_color="+id_body_color+"&id_branch="+id_branch,true);
    xmlhttp.send();



}


