
/*
    Авторизация
 */

$('.login-btn').click(function (e) {
    e.preventDefault(); //страница не будет обновляться

    $(`input`).removeClass('error');

    let login = $(`input[name = "login"]`).val();
    let password = $(`input[name = "password"]`).val();


    $.ajax({
        url: '../../vendor/authorization/signin.php',
        type: 'POST',
        dataType: 'json',
        data: {
            login: login,
            password: password,

        },
        success (data) {
            if (data.status) {
                document.location.href = '../../views/pages/index.php';
            } else {

                if (data.type === 1) {
                    data.fields.forEach(function (field) {
                        $(`input[name = "${field}"]`).addClass('error');
                    });

                }

                $('.msg').removeClass('none').text(data.message);
            }
        }
    });

});


/*
    Регистрация
 */

$('.register-btn').click(function (e) {
    e.preventDefault(); //страница не будет обновляться

    $(`input`).removeClass('error');

    let full_name = $(`input[name = "full_name"]`).val();
    let email = $(`input[name = "email"]`).val();
    let phone = $(`input[name = "phone"]`).val();
    let login = $(`input[name = "login"]`).val();
    let password = $(`input[name = "password"]`).val();
    let password_confirm = $(`input[name = "password_confirm"]`).val();

    /*
    let formData = new FormData();
    formData.append('full_name', full_name);
    formData.append('email', email);
    formData.append('login', login);
    formData.append('password', password);
    formData.append('password_confirm', password_confirm);
*/
    $.ajax({
        url: '../../vendor/authorization/signup.php',
        type: 'POST',
        dataType: 'json',
        data: {
            full_name: full_name,
            email: email,
            phone: phone,
            login: login,
            password: password,
            password_confirm: password_confirm
        },
        success: function (data) {
            if (data.status) {

                document.location.href = 'login.php';

            } else {

                if (data.type === 1) {
                    data.fields.forEach(function (field) {
                        $(`input[name = "${field}"]`).addClass('error');
                    });

                }

                $('.msg').removeClass('none').text(data.message);
            }
        }
    });
});