function addNewCar(id_manager) {
    if (id_manager == "") {
        document.getElementById("brandList").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("brandList").innerHTML = this.responseText;
            }
        };

        xmlhttp.open("GET","../../vendor/addCar/getBrand.php?id_manager="+id_manager,true);
        xmlhttp.send();
    }
}

function getModel(id_manager, id_stamp) {
    if (id_stamp == "") {
        document.getElementById("modelList").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("modelList").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","../../vendor/addCar/getModel.php?id_manager="+id_manager+"&id_stamp="+id_stamp,true);
        xmlhttp.send();
    }
}

function getEquipment(id_manager, id_stamp, id_model) {
    if (id_model == "") {
        document.getElementById("equipmentList").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("equipmentList").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","../../vendor/addCar/getEquipment.php?id_manager="+id_manager+"&id_stamp="+id_stamp+"&id_model="+id_model,true);
        xmlhttp.send();
    }
}

function getColorEquipment(id_manager, id_stamp, id_model, id_equipment) {
    if (id_equipment == "") {
        document.getElementById("colorEquipmentList").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("colorEquipmentList").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","../../vendor/addCar/getColorCar.php?id_manager="+id_manager+"&id_stamp="+id_stamp+"&id_model="+id_model+"&id_equipment="+id_equipment,true);
        xmlhttp.send();
    }
}

function getVIN(id_manager, id_stamp, id_model, id_equipment, id_body_color) {
    if (id_body_color == "") {
        document.getElementById("formVIN").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("formVIN").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","../../vendor/addCar/getVIN.php?id_manager="+id_manager+"&id_stamp="+id_stamp+"&id_model="+id_model+"&id_equipment="+id_equipment+"&id_body_color="+id_body_color,true);
        xmlhttp.send();
    }
}

function makeNewCar(id_manager, id_stamp, id_model, id_equipment, id_body_color) {
    var vin = document.getElementById("input_car_vin").value;

        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                alert(this.responseText);
                //location.href = "index.php";
                //document.getElementById("answerAddCar").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","../../vendor/addCar/addNewCar.php?id_manager="+id_manager+"&id_stamp="+id_stamp+"&id_model="+id_model+"&id_equipment="+id_equipment+"&id_body_color="+id_body_color+"&vin="+vin,true);
        xmlhttp.send();
}