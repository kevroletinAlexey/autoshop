function setCookie(id_city) {
    if (id_city == "") {
        document.getElementById("setCookieList").innerHTML = "";
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("setCookieList").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","../../vendor/setCookie.php?id_city="+id_city,true);
        xmlhttp.send();
    }
}

