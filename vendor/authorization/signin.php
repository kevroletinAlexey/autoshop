<?php
    session_start();

    require_once '/Applications/MAMP/htdocs/vendor/config/connect.php';

    $login = $_POST['login'];
    $password = $_POST['password'];



    /* Валидация */

    $error_fields = [];

    if ($login === '') {
        $error_fields[] = 'login';
    }

    if ($password === '') {
        $error_fields[] = 'password';
    }

    if (!empty($error_fields)) {
        $response = [
            "status" => false,
            "type" => 1,
            "message" => "Проверьте правильность полей",
            "fields" => $error_fields
        ];

        echo json_encode($response);

        die();
    }

    /* Попытка входа */

    $password = md5($password);

    $check_user = mysqli_query($connect, "SELECT * FROM `user` WHERE `login` = '$login' AND `password` = '$password'");
    $check_manager = mysqli_query($connect, "SELECT * FROM `manager` WHERE `login` = '$login' AND `password` = '$password'");

    if (mysqli_num_rows($check_user) > 0) {

        $user = mysqli_fetch_assoc($check_user);

        $_SESSION['user'] = [
            'id' => $user['id_user'],
            'full_name' => $user['full_name'],
            'email' => $user['email']
        ];

        $response = [
            "status" => true
        ];

        echo json_encode($response);

    } elseif (mysqli_num_rows($check_manager) > 0) {

        $manager = mysqli_fetch_assoc($check_manager);

        $_SESSION['manager'] = [
            'id' => $manager['id_manager'],
            'last_name' => $manager['last_name'],
            'first_name' => $manager['first_name'],
            'patronymic' => $manager['patronymic'],
            'id_branch' => $manager['id_branch']
        ];

        $response = [
            "status" => true
        ];

        echo json_encode($response);

    } else {

        $response = [
            "status" => false,
            "message" => "Неверный логин или пароль"
        ];

        echo json_encode($response);

    }