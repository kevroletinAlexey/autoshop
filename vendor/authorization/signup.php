<?php
    session_start();

    require_once '/Applications/MAMP/htdocs/vendor/config/connect.php';

    $full_name = $_POST['full_name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $login = $_POST['login'];
    $password = $_POST['password'];
    $password_confirm = $_POST['password_confirm'];

    /* Валидация */

    $check_login = mysqli_query($connect, "SELECT * FROM `user` WHERE `login` = '$login'");
    if (mysqli_num_rows($check_login) > 0) {
        $response = [
            "status" => false,
            "type" => 1,
            "message" => "Такой логин уже существует",
            "fields" => ['login']
        ];
        echo json_encode($response);
        die();
    }

    $error_fields = [];

    if ($full_name === '') {
        $error_fields[] = 'full_name';
    }

    if ($email === '' || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error_fields[] = 'email';
    }

    if ($phone === '') {
        $error_fields[] = 'phone';
    }

    if ($login === '') {
        $error_fields[] = 'login';
    }

    if ($password === '') {
        $error_fields[] = 'password';
    }

    if ($password_confirm === '') {
        $error_fields[] = 'password_confirm';
    }

    if (!empty($error_fields)) {
        $response = [
            "status" => false,
            "type" => 1,
            "message" => "Проверьте правильность полей",
            "fields" => $error_fields
        ];

        echo json_encode($response);

        die();
    }

    /* Проверка пароля и запись в users */

    if ($password == $password_confirm) {

        $password = md5($password);

        $query ="INSERT INTO `user` (`id_user`, `full_name`, `login`, `email`, `phone`, `password`) VALUES (NULL, '$full_name', '$login', '$email', '$phone', '$password')";
        mysqli_query($connect, $query);

        $response = [
            "status" => true,
            "message" => "Регистрация прошла успешно",
        ];

        echo json_encode($response);

    } else {
        $response = [
            "status" => false,
            "message" => "Пароли не совпадают",
        ];

        echo json_encode($response);
    }

