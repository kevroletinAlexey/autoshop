<?php
    session_start();

if(isset($_SESSION['manager'])){
    if(isset($_GET['logout_id'])){
        session_unset();
        session_destroy();
        header("Location: ../../views/pages/index.php");
    }else{
        unset($_SESSION['manager']);
        header("Location: ../../views/pages/index.php");
    }
}
elseif(isset($_SESSION['user'])){
    if(isset($_GET['logout_id'])){
        session_unset();
        session_destroy();
        header("Location: ../../views/pages/index.php");
    }else{
        unset($_SESSION['user']);
        header("Location: ../../views/pages/index.php");
    }
}
else{
    header("Location: ../../views/pages/index.php");
}
