<?php

session_start();

require_once '/Applications/MAMP/htdocs/vendor/config/connect.php';

$id_city = $_GET['id_city'];

if (!isset($_COOKIE["id_city"])) {
    setcookie("id_city", $id_city, time() + 604800, "/"); //7 day
    echo "Город успешно сохранен в cookie";
} else {
    setcookie ("id_city", "", time() - 3600);
    setcookie("id_city", $id_city, time() + 604800, "/"); //7 day
    echo "Город успешно изменен";
}


