<?php
session_start();

require_once '/Applications/MAMP/htdocs/vendor/config/connect.php';

$id_equipment = $_GET['id_equipment'];
$id_body_color = $_GET['id_body_color'];

$id_city = $_COOKIE['id_city'];

//получим имя города из куки
$query_name_city = "SELECT name FROM city WHERE id_city = ".$id_city.";";
$services_name_city = mysqli_query($connect, $query_name_city);
$row_name_city = mysqli_fetch_array($services_name_city);
$city_name = $row_name_city['name'];

$query = "
    SELECT DISTINCT a.id_address, c.name AS city_name, street, house, b.id_branch AS id_branch
    FROM car_branch
        JOIN equipment e on car_branch.id_equipment = e.id_equipment
        JOIN status_car_branch scb on car_branch.id_status_car_branch = scb.id_status_car_branch
        JOIN branch b on car_branch.id_branch = b.id_branch
        JOIN address a on b.id_address = a.id_address
        JOIN city c on a.id_city = c.id_city
    WHERE e.id_equipment = ".$id_equipment." AND scb.id_status_car_branch = 1 AND car_branch.id_body_color = ".$id_body_color." AND c.id_city = ".$id_city.";";

$services = mysqli_query($connect, $query);

//проверяем есть ли авто в городе из куки
if (mysqli_num_rows($services) > 0) {
    echo "<select name='branch' onchange='getAdditionalService(".$id_equipment.", ".$id_body_color.", this.value)'>
            <option value=''>Выберите филиал в городе ".$city_name.":</option>";

    while ($row = mysqli_fetch_array($services)) {
        echo "<option value=".$row['id_branch'].">".$row['city_name']." ".$row['street']." ".$row['house']."</option>";
    }

    echo "</select>
            <div id='additionalServiceList'></div>";
} else {
    //если нет то выводим все остальные города
    $query = "
    SELECT DISTINCT a.id_address, c.name AS city_name, street, house, b.id_branch AS id_branch
    FROM car_branch
        JOIN equipment e on car_branch.id_equipment = e.id_equipment
        JOIN status_car_branch scb on car_branch.id_status_car_branch = scb.id_status_car_branch
        JOIN branch b on car_branch.id_branch = b.id_branch
        JOIN address a on b.id_address = a.id_address
        JOIN city c on a.id_city = c.id_city
    WHERE e.id_equipment = ".$id_equipment." AND scb.id_status_car_branch = 1 AND car_branch.id_body_color = ".$id_body_color.";";

    $services = mysqli_query($connect, $query);

    echo "<select name='branch' onchange='getAdditionalService(".$id_equipment.", ".$id_body_color.", this.value)'>
            <option value=''>В городе ".$city_name." отсутсвует данная комплектация. Вы можете выбрать филиал в другом городе:</option>";

    while ($row = mysqli_fetch_array($services)) {
        echo "<option value=".$row['id_branch'].">".$row['city_name']." ".$row['street']." ".$row['house']."</option>";
    }

    echo "</select>
            <div id='additionalServiceList'></div>";
}



