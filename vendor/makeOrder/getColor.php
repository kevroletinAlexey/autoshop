<?php

session_start();

require_once '/Applications/MAMP/htdocs/vendor/config/connect.php';

$id_equipment = $_GET['id_equipment'];

$query = "
        SELECT value, bc.id_body_color AS id_body_color
        FROM equipment_body_color
                 JOIN body_color bc on equipment_body_color.id_body_color = bc.id_body_color
        WHERE id_equipment = ".$id_equipment.";";

$services = mysqli_query($connect, $query);
?>

<select name='color' onchange="getBranch(<?=$id_equipment?>, this.value)">
    <option value="">Выберите цвет кузова:</option>
    <?php
    while ($row = mysqli_fetch_array($services)) {
        echo "<option value=".$row['id_body_color'].">".$row['value']."</option>";
    }
    ?>
</select>

<div id="branchList"></div>