<?php

session_start();

require_once '/Applications/MAMP/htdocs/vendor/config/connect.php';

$id_manager = $_GET['id_manager'];

$query = "SELECT id_stamp, name FROM stamp;";

$services = mysqli_query($connect, $query);
?>

<select name='brand' onchange="getModel(<?=$id_manager?>, this.value)">
    <option value="">Выберите марку:</option>
    <?php
    while ($row = mysqli_fetch_array($services)) {
        echo "<option value=".$row['id_stamp'].">".$row['name']."</option>";
    }
    ?>
</select>

<div id="modelList"></div>