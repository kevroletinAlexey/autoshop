
<?php

session_start();

require_once '/Applications/MAMP/htdocs/vendor/config/connect.php';

$id_manager = $_GET['id_manager'];
$id_stamp = $_GET['id_stamp'];
$id_model = $_GET['id_model'];
$id_equipment = $_GET['id_equipment'];
$id_body_color = $_GET['id_body_color'];
$vin = $_GET['vin'];



$id_status_car_branch = 1; //в наличии

//получить id_branch
$query_branch = "SELECT id_branch FROM manager WHERE id_manager = ".$id_manager.";";
$services_branch = mysqli_query($connect, $query_branch);
$row_branch = mysqli_fetch_array($services_branch);
$id_branch = $row_branch['id_branch'];


//проверка уникальности vin
$query_vin = "SELECT * FROM car_branch WHERE vin = '".$vin."';";
$check_vin = mysqli_query($connect, $query_vin);


if (mysqli_num_rows($check_vin) == 0) {

    //добавить автомобиль
    $query = "INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) 
            VALUES (" . $id_equipment . ", " . $id_branch . ", " . $id_body_color . ", '" . $vin . "', " . $id_status_car_branch . ");";
    $services = mysqli_query($connect, $query);

    //вывести сообщение
    $query_name_car = "SELECT s.name as stamp_name, m.name as model_name, equipment.name as equipment_name FROM equipment
                        JOIN model m on equipment.id_model = m.id_model
                        JOIN stamp s on m.id_stamp = s.id_stamp
                        WHERE equipment.id_equipment = ".$id_equipment.";";
    $services_name_car = mysqli_query($connect, $query_name_car);
    $row_name_car = mysqli_fetch_array($services_name_car);

    $query_color = "SELECT value FROM body_color
                    WHERE id_body_color = ".$id_body_color.";";
    $services_color = mysqli_query($connect, $query_color);
    $row_color = mysqli_fetch_array($services_color);

    $answer = "Автомобиль ".$row_name_car['stamp_name']." ".$row_name_car['model_name']." ".$row_name_car['equipment_name'].". Цвет кузова: ".$row_color['value'].", VIN: ".$vin." Успешно добавлен в базу данных.";
    echo $answer;

} else {
    $answer = "Автомобиль с введенным VIN номером уже добавлен в базу данных.";
    echo $answer;
}

