<?php

session_start();

require_once '/Applications/MAMP/htdocs/vendor/config/connect.php';

$id_manager = $_GET['id_manager'];
$id_stamp = $_GET['id_stamp'];
$id_model = $_GET['id_model'];
$id_equipment = $_GET['id_equipment'];

$query = "SELECT bc.id_body_color as id_body_color, bc.value as color FROM equipment_body_color AS ebc
JOIN body_color bc on ebc.id_body_color = bc.id_body_color
WHERE ebc.id_equipment = ".$id_equipment.";";

$services = mysqli_query($connect, $query);

?>

<select name='colorEquipment' onchange="getVIN(<?=$id_manager?>, <?=$id_stamp?>, <?=$id_model?>, <?=$id_equipment?>, this.value)">
    <option value="">Выберите цвет кузова:</option>
    <?php
    while ($row = mysqli_fetch_array($services)) {
        echo "<option value=".$row['id_body_color'].">".$row['color']."</option>";
    }
    ?>
</select>
<div id="formVIN"></div>