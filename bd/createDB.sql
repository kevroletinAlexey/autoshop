CREATE TABLE country (
    id_country int AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) NOT NULL
);

CREATE TABLE city (
    id_city int AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) NOT NULL
);

CREATE TABLE address (
    id_address int AUTO_INCREMENT PRIMARY KEY,
    id_country int,
    id_city int,
    street varchar(255),
    house varchar(50),
    flat  varchar(50),
    postal_code varchar(50),
    FOREIGN KEY (id_country) REFERENCES country(id_country) ON DELETE RESTRICT,
    FOREIGN KEY (id_city) REFERENCES city(id_city) ON DELETE RESTRICT
);

CREATE TABLE user (
    id_user int AUTO_INCREMENT PRIMARY KEY,
    full_name varchar(255) NOT NULL,
    login varchar(255) NOT NULL,
    password varchar(500) NOT NULL,
    email varchar(255) NOT NULL,
    phone varchar(128)
);

CREATE TABLE branch (
    id_branch int AUTO_INCREMENT PRIMARY KEY,
    name varchar(300),
    phone varchar(128),
    id_address int,
    FOREIGN KEY (id_address) REFERENCES address(id_address) ON DELETE RESTRICT
);

CREATE TABLE manager (
    id_manager int AUTO_INCREMENT PRIMARY KEY,
    last_name varchar(255) NOT NULL,
    first_name varchar(255) NOT NULL,
    patronymic varchar(255) DEFAULT NULL,
    login varchar(255) NOT NULL,
    password varchar(500) NOT NULL,
    email varchar(255) NOT NULL,
    phone varchar(128) NOT NULL,
    post varchar(255) NOT NULL,
    birth_day date NOT NULL,
    id_branch int,
    FOREIGN KEY (id_branch) REFERENCES branch(id_branch) ON DELETE CASCADE
);

CREATE TABLE stamp (
    id_stamp int AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) NOT NULL,
    id_country int,
    FOREIGN KEY (id_country) REFERENCES country(id_country) ON DELETE RESTRICT
);

CREATE TABLE body_type (
    id_body_type int AUTO_INCREMENT PRIMARY KEY,
    value varchar(255) NOT NULL
);

CREATE TABLE vehicle_category (
    id_vehicle_category int AUTO_INCREMENT PRIMARY KEY,
    value varchar(255) NOT NULL
);

CREATE TABLE model (
    id_model int AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) NOT NULL,
    id_stamp int,
    id_vehicle_category int,
    FOREIGN KEY (id_stamp) REFERENCES stamp(id_stamp) ON DELETE CASCADE,
    FOREIGN KEY (id_vehicle_category) REFERENCES vehicle_category(id_vehicle_category) ON DELETE CASCADE
);

CREATE TABLE body_color (
    id_body_color int AUTO_INCREMENT PRIMARY KEY,
    value varchar(255) NOT NULL
);

CREATE TABLE equipment (
    id_equipment int AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) NOT NULL,
    id_model int,
    price decimal(15,2) NOT NULL,
    id_body_type int,
    description text,
    FOREIGN KEY (id_model) REFERENCES model(id_model) ON DELETE CASCADE,
    FOREIGN KEY (id_body_type) REFERENCES body_type(id_body_type) ON DELETE CASCADE
);

CREATE TABLE photo (
    id_photo int AUTO_INCREMENT PRIMARY KEY,
    link varchar(350) NOT NULL,
    id_equipment int,
    FOREIGN KEY (id_equipment) REFERENCES equipment(id_equipment) ON DELETE CASCADE
);

CREATE TABLE equipment_body_color (
    id_equipment int,
    id_body_color int,
    FOREIGN KEY (id_equipment) REFERENCES equipment(id_equipment) ON DELETE CASCADE,
    FOREIGN KEY (id_body_color) REFERENCES body_color(id_body_color) ON DELETE CASCADE,
    PRIMARY KEY (id_equipment, id_body_color)
);

CREATE TABLE property (
    id_property int AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) NOT NULL
);

CREATE TABLE equipment_property (
    id_equipment int,
    id_property int,
    value varchar(255),
    FOREIGN KEY (id_equipment) REFERENCES equipment(id_equipment) ON DELETE CASCADE,
    FOREIGN KEY (id_property) REFERENCES property(id_property) ON DELETE CASCADE,
    PRIMARY KEY (id_equipment, id_property)
);

CREATE TABLE status_car_branch (
    id_status_car_branch int AUTO_INCREMENT PRIMARY KEY,
    value varchar(255) NOT NULL
);

CREATE TABLE car_branch (
    id_car_branch int AUTO_INCREMENT PRIMARY KEY,
    id_equipment int,
    id_branch int,
    id_body_color int,
    vin varchar(255) NOT NULL,
    id_status_car_branch int,
    FOREIGN KEY (id_equipment) REFERENCES equipment(id_equipment) ON DELETE CASCADE,
    FOREIGN KEY (id_branch) REFERENCES branch(id_branch) ON DELETE CASCADE,
    FOREIGN KEY (id_body_color) REFERENCES body_color(id_body_color) ON DELETE RESTRICT,
    FOREIGN KEY (id_status_car_branch) REFERENCES status_car_branch(id_status_car_branch) ON DELETE RESTRICT
);

CREATE TABLE client_order (
    id_client_order int AUTO_INCREMENT PRIMARY KEY,
    id_manager int,
    id_user int,
    id_car_branch int,
    data_create datetime,
    FOREIGN KEY (id_manager) REFERENCES manager(id_manager) ON DELETE RESTRICT,
    FOREIGN KEY (id_user) REFERENCES user(id_user) ON DELETE RESTRICT,
    FOREIGN KEY (id_car_branch) REFERENCES car_branch(id_car_branch) ON DELETE RESTRICT
);


CREATE TABLE additional_service (
    id_additional_service int AUTO_INCREMENT PRIMARY KEY,
    name varchar(255) NOT NULL
);

CREATE TABLE equipment_additional_service (
    id_additional_service int,
    id_equipment int,
    price varchar(255) NOT NULL,
    FOREIGN KEY (id_additional_service) REFERENCES additional_service(id_additional_service) ON DELETE CASCADE,
    FOREIGN KEY (id_equipment) REFERENCES equipment(id_equipment) ON DELETE CASCADE,
    PRIMARY KEY (id_additional_service, id_equipment)
);

CREATE TABLE client_order_additional_service (
    id_additional_service int,
    id_client_order int,
    FOREIGN KEY (id_additional_service) REFERENCES additional_service(id_additional_service) ON DELETE RESTRICT,
    FOREIGN KEY (id_client_order) REFERENCES client_order(id_client_order) ON DELETE CASCADE,
    PRIMARY KEY (id_client_order, id_additional_service)
);

