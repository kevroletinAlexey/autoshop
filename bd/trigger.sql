DELIMITER //
CREATE TRIGGER updateStatus
    AFTER INSERT
    ON client_order
    FOR EACH ROW
    UPDATE car_branch SET id_status_car_branch = 3 WHERE id_car_branch = NEW.id_car_branch//
DELIMITER ;