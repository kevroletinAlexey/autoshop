INSERT INTO city(name) VALUES ('Москва');
INSERT INTO city(name) VALUES ('Санкт-Петербург');
INSERT INTO city(name) VALUES ('Казань');

INSERT INTO country(name) VALUES ('Россия');
INSERT INTO country(name) VALUES ('Германия');
INSERT INTO country(name) VALUES ('США');

INSERT INTO stamp(name, id_country) VALUES ('Porsche', 2);
INSERT INTO stamp(name, id_country) VALUES ('Jeep', 3);



INSERT INTO model(name, id_stamp, id_vehicle_category) VALUES ('911', 1, 8);
INSERT INTO model(name, id_stamp, id_vehicle_category) VALUES ('718', 1, 8);
INSERT INTO model(name, id_stamp, id_vehicle_category) VALUES ('Taycan', 1, 8);
INSERT INTO model(name, id_stamp, id_vehicle_category) VALUES ('Panamera', 1, 8);

INSERT INTO model(name, id_stamp, id_vehicle_category) VALUES ('WRANGLER', 2, 8);
INSERT INTO model(name, id_stamp, id_vehicle_category) VALUES ('Grand Cherokee', 2, 8);

INSERT INTO equipment(name, id_model, price, id_body_type, description) VALUES ('RUBICON', 5, 5895000, 11, 'Новый Jeep® Wrangler Rubicon специально разработан для самых экстремальных приключений . С 32-дюймовыми внедорожными колесами, передаточным отношением в трансмиссии 77.2 и легкими в использовании органами управления полным приводом - Rubicon полностью подготовлен к бездорожью.');

INSERT INTO equipment(name, id_model, price, id_body_type, description) VALUES ('Carrera', 1, 8790000, 8, 'Ферри Porsche и его сын Фердинанд Александр создали модель 911 – икону спортивного автомобиля, которая на протяжении более чем 50 лет вызывает невероятный восторг. Секрет успеха: гармоничное сочетание неподвластного времени дизайна и уникальной – как говорят наши инженеры – компоновки. Посадочная формула 2+2 представляет собой идеальное решение для чистокровного компактного спортивного автомобиля, который также в полной мере соответствует требованиям повседневной эксплуатации. Компактный двигатель располагается в задней части, что способствует отличной тяге и определяет собой то неподражаемое чувство, которое рождается у человека за рулем 911. С 1963 года мы каждый день работаем над тем, чтобы сделать 911 еще более совершенным. И еще никогда мы не были так близки к своей цели. Восьмое поколение 911 воплощает в себе все наилучшие качества своих предшественников и тем самым отражает как уважение к традициям, так и устремленность в будущее. Силуэт – культовый. Дизайн – неподвластный времени. Техника – рожденная на гоночных трассах и всегда идущая на шаг впереди. Результатом стал 911 в своем самом красивом и современном исполнении. Со множеством исторических отсылок и при этом демонстрирующий будущее спортивного автомобиля. Тем самым 911 становится автомобилем вне времени. 911. Вне времени');
INSERT INTO equipment(name, id_model, price, id_body_type, description) VALUES ('Carrera Cabriolet', 1, 9760000, 6, 'Ферри Porsche и его сын Фердинанд Александр создали модель 911 – икону спортивного автомобиля, которая на протяжении более чем 50 лет вызывает невероятный восторг. Секрет успеха: гармоничное сочетание неподвластного времени дизайна и уникальной – как говорят наши инженеры – компоновки. Посадочная формула 2+2 представляет собой идеальное решение для чистокровного компактного спортивного автомобиля, который также в полной мере соответствует требованиям повседневной эксплуатации. Компактный двигатель располагается в задней части, что способствует отличной тяге и определяет собой то неподражаемое чувство, которое рождается у человека за рулем 911. С 1963 года мы каждый день работаем над тем, чтобы сделать 911 еще более совершенным. И еще никогда мы не были так близки к своей цели. Восьмое поколение 911 воплощает в себе все наилучшие качества своих предшественников и тем самым отражает как уважение к традициям, так и устремленность в будущее. Силуэт – культовый. Дизайн – неподвластный времени. Техника – рожденная на гоночных трассах и всегда идущая на шаг впереди. Результатом стал 911 в своем самом красивом и современном исполнении. Со множеством исторических отсылок и при этом демонстрирующий будущее спортивного автомобиля. Тем самым 911 становится автомобилем вне времени.');
INSERT INTO equipment(name, id_model, price, id_body_type, description) VALUES ('Carrera 4', 1, 9370000, 8, 'Ферри Porsche и его сын Фердинанд Александр создали модель 911 – икону спортивного автомобиля, которая на протяжении более чем 50 лет вызывает невероятный восторг. Секрет успеха: гармоничное сочетание неподвластного времени дизайна и уникальной – как говорят наши инженеры – компоновки. Посадочная формула 2+2 представляет собой идеальное решение для чистокровного компактного спортивного автомобиля, который также в полной мере соответствует требованиям повседневной эксплуатации. Компактный двигатель располагается в задней части, что способствует отличной тяге и определяет собой то неподражаемое чувство, которое рождается у человека за рулем 911. С 1963 года мы каждый день работаем над тем, чтобы сделать 911 еще более совершенным. И еще никогда мы не были так близки к своей цели.');
INSERT INTO equipment(name, id_model, price, id_body_type, description) VALUES ('Carrera GTS', 1, 11480000, 8, 'Внешность Carrera GTS 2022 свидетельствует о сильном характере настоящего лидера. Спортивную генетику автомобиля подчеркивает приземистый силуэт, обтекаемые массивные крылья, колеса разного диаметра на передней и задней осях. Еще один выразительный элемент дизайна – покатый капот с еле заметными фигурными элементами. Новый Carrera GTS выглядит потрясающе элегантно!');
INSERT INTO equipment(name, id_model, price, id_body_type, description) VALUES ('Carrera 4S', 1, 10470000, 8, 'Ферри Porsche и его сын Фердинанд Александр создали модель 911 – икону спортивного автомобиля, которая на протяжении более чем 50 лет вызывает невероятный восторг. Секрет успеха: гармоничное сочетание неподвластного времени дизайна и уникальной – как говорят наши инженеры – компоновки. Посадочная формула 2+2 представляет собой идеальное решение для чистокровного компактного спортивного автомобиля, который также в полной мере соответствует требованиям повседневной эксплуатации. Компактный двигатель располагается в задней части, что способствует отличной тяге и определяет собой то неподражаемое чувство, которое рождается у человека за рулем 911. С 1963 года мы каждый день работаем над тем, чтобы сделать 911 еще более совершенным. И еще никогда мы не были так близки к своей цели.');

INSERT INTO equipment(name, id_model, price, id_body_type, description) VALUES ('Cayman', 2, 5060000, 8, 'Второе поколение Porsche Cayman было представлено в 2013 году. Модель появилась на свет благодаря желанию руководства компании создать копию модели Boxster более подходящую для повседневных поездок, сохранив при этом все драйверские особенности автомобиля. Но на самом деле, изменения оказались более значительными, чем предполагалось ранее.');
INSERT INTO equipment(name, id_model, price, id_body_type, description) VALUES ('Boxster', 2, 5140000, 8, 'Модели 718 созданы для жизни в спортивном стиле. Это среднемоторные спортивные автомобили, которые хранят в себе спортивный дух легендарного Porsche 718 и отличаются современной техникой, демонстрируя это потрясающее сочетание на дорогах нашего мира. С одной лишь целью: добавить эмоции в будни.');

INSERT INTO equipment(name, id_model, price, id_body_type, description) VALUES ('4 Cross Turismo', 3, 7790000, 12, 'Приехать домой и зарядить батарею: Porsche E-Performance предлагает Вам продуманные и комплексные решения для зарядки Вашего спортивного электромобиля.
Все они адаптированы к Вашим личным пожеланиям и условиям. Все именно так, как нужно Вам – в Вашей повседневной жизни.
При этом мы имеем в виду не только подходящие аксессуары для зарядки, но и полезные сервисы и приложения, как например приложение Porsche Connect.
Тем самым, даже отдыхая, Вы сможете сохранить функциональность своего автомобиля.');

INSERT INTO equipment(name, id_model, price, id_body_type, description) VALUES ('4S Sport Turismo', 4, 10150000, 12, '');

INSERT INTO photo(link, id_equipment) VALUES ('assets/img/444386_pOGoX1jpu5DcUyBoXuJpglFN4jkOEeIT.jpg.optimized.png', 1);
INSERT INTO photo(link, id_equipment) VALUES ('assets/img/444388_FEMmm1VJxQomJ88e6yQxov8cwfSLthMY.jpg.optimized.png', 2);
INSERT INTO photo(link, id_equipment) VALUES ('assets/img/454328_LEDKsjBVSarHwfKInSdyMuhPDFcxohQb.jpg.optimized.png', 3);
INSERT INTO photo(link, id_equipment) VALUES ('assets/img/465373_x4NdJrFcPDDav8jRNmHCuUvMUAXjAvoY.jpg.optimized.png', 4);
INSERT INTO photo(link, id_equipment) VALUES ('assets/img/444395_fQX81FEAauTVDu3BQCw6M22NlBPeQIBV.jpg.optimized.png', 5);
INSERT INTO photo(link, id_equipment) VALUES ('assets/img/445380_A8XQdobKQAHGt2KqpNNytevBm3LJKjUy.jpg.optimized.png', 6);
INSERT INTO photo(link, id_equipment) VALUES ('assets/img/445391_7DbpUEorhoUPEOulHYP0QjCCnuIiE1gq.jpg.optimized.jpeg', 7);
INSERT INTO photo(link, id_equipment) VALUES ('assets/img/457089_2fcoYcRk614T1cpAPQQt3g2tnO5IOnBq.jpg.optimized.jpeg', 8);
INSERT INTO photo(link, id_equipment) VALUES ('assets/img/456948_4ajslUd8YWmpQuLG3ROGYjx2m8REHl4l.jpg.optimized.jpeg', 9);

INSERT INTO photo(link, id_equipment) VALUES('assets/img/firecracker-red.png', 10);

INSERT INTO property(name) VALUES ('Мощность');
INSERT INTO property(name) VALUES ('Разгон 0-100 км/ч');
INSERT INTO property(name) VALUES ('Максимальная скорость');

INSERT INTO body_color(value) VALUES ('Белый');
INSERT INTO body_color(value) VALUES ('Черный');
INSERT INTO body_color(value) VALUES ('Красный');
INSERT INTO body_color(value) VALUES ('Желтый');

INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (10, 1);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (10, 2);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (10, 3);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (10, 4);

INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (1, 1);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (1, 2);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (1, 3);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (1, 4);

INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (2, 1);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (2, 2);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (2, 3);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (2, 4);

INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (3, 1);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (3, 2);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (3, 3);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (3, 4);

INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (4, 1);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (4, 2);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (4, 3);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (4, 4);

INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (5, 1);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (5, 2);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (5, 3);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (5, 4);

INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (6, 1);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (6, 2);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (6, 3);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (6, 4);

INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (7, 1);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (7, 2);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (7, 3);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (7, 4);

INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (8, 1);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (8, 2);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (9, 3);
INSERT INTO equipment_body_color(id_equipment, id_body_color) VALUES (9, 4);

INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (10, 1, '227 л.с.');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (10, 2, '6.8 с');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (10, 3, '180 км/ч');

INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (1, 1, '385 л.с./283 кВт');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (1, 2, '4.2 с');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (1, 3, '293 км/ч');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (2, 1, '385 л.с./283 кВт');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (2, 2, '4.4 с');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (2, 3, '291 км/ч');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (3, 1, '385 л.с./283 кВт');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (3, 2, '4.2 с');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (3, 3, '291 км/ч');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (4, 1, '480 л.с./353 кВт');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (4, 2, '3.4 с');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (4, 3, '311 км/ч');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (5, 1, '450 л.с./331 кВт');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (5, 2, '3.6 с');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (5, 3, '306 км/ч');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (6, 1, '300 л.с./220 кВт');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (6, 2, '5.3 с');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (6, 3, '275 км/ч');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (7, 1, '300 л.с./220 кВт');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (7, 2, '5.3 с');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (7, 3, '275 км/ч');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (8, 1, '380 л.с./280 кВт');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (8, 2, '5.1 с');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (8, 3, '220 км/ч');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (9, 1, '450 л.с./331 кВт');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (9, 2, '4.3 с');
INSERT INTO equipment_property(id_equipment, id_property, value) VALUES (9, 3, '289 км/ч');

INSERT INTO address(id_country, id_city, street, house) VALUES (1, 1, 'Ленинский проспект', 107);
INSERT INTO address(id_country, id_city, street, house) VALUES (1, 1, 'ул. Советская', 91);

INSERT INTO branch(name, phone, id_address) VALUES ('Дилерский центр в Москве 1', '89887882324', 1);
INSERT INTO branch(name, phone, id_address) VALUES ('Дилерский центр в Москве 2', '89887882300', 2);

INSERT INTO status_car_branch(value) VALUES ('В наличии');
INSERT INTO status_car_branch(value) VALUES ('Забронирован');
INSERT INTO status_car_branch(value) VALUES ('Заказан');
INSERT INTO status_car_branch(value) VALUES ('Продан');

INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (10, 1, 1, 'JD0ZZZ99Z5S73001', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (10, 1, 2, 'JD0ZZZ99Z5S73001', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (10, 1, 3, 'JD0ZZZ99Z5S73001', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (10, 2, 4, 'JD0ZZZ99Z5S73001', 1);

INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (1, 1, 1, 'WP0ZZZ99Z5S731111', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (1, 1, 2, 'WP0ZZZ99Z5S731112', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (1, 1, 3, 'WP0ZZZ99Z5S731113', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (1, 1, 4, 'WP0ZZZ99Z5S731114', 1);

INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (1, 1, 1, 'WP0ZZZ99Z5S731118', 1);

INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (2, 1, 1, 'WP0ZZZ99Z5S730000', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (3, 1, 1, 'WP0ZZZ99Z5S731253', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (4, 1, 1, 'WP0ZZZ99Z5S734321', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (5, 1, 1, 'WP0ZZZ99Z5S731234', 1);

INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (1, 2, 1, 'WP0ZZZ99Z5S736666', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (2, 2, 1, 'WP0ZZZ99Z5S737777', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (3, 2, 2, 'WP0ZZZ99Z5S738888', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (4, 2, 1, 'WP0ZZZ99Z5S731321', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (5, 2, 1, 'WP0ZZZ99Z5S733333', 1);

INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (6, 1, 1, 'WP0ZZZ96ZRS811212', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (7, 1, 1, 'WP0ZZZ96ZRS811111', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (8, 1, 1, 'WP1AF2A51FLB90011', 1);
INSERT INTO car_branch(id_equipment, id_branch, id_body_color, vin, id_status_car_branch) VALUES (9, 1, 1, 'WP0ZZZ97ZAL089997', 1);

INSERT INTO additional_service(name) VALUES ('Противоугонный комплекс');
INSERT INTO additional_service(name) VALUES ('Расширенная гарантия');
INSERT INTO additional_service(name) VALUES ('Урегулирование убытков по КАСКО');
INSERT INTO additional_service(name) VALUES ('Пленка');

INSERT INTO equipment_additional_service(id_additional_service, id_equipment, price) VALUES (1, 1, '100000');
INSERT INTO equipment_additional_service(id_additional_service, id_equipment, price) VALUES (2, 1, '200000');
INSERT INTO equipment_additional_service(id_additional_service, id_equipment, price) VALUES (3, 1, '50000');
INSERT INTO equipment_additional_service(id_additional_service, id_equipment, price) VALUES (4, 1, '90000');

INSERT INTO equipment_additional_service(id_additional_service, id_equipment, price) VALUES (1, 2, '100000');
INSERT INTO equipment_additional_service(id_additional_service, id_equipment, price) VALUES (2, 2, '200000');
INSERT INTO equipment_additional_service(id_additional_service, id_equipment, price) VALUES (3, 2, '50000');
INSERT INTO equipment_additional_service(id_additional_service, id_equipment, price) VALUES (4, 2, '90000');
